.PHONY: all

PYTHON_BASE_IMAGE=python
PYTHON_BASE_IMAGE_VERSION=3.6.2-slim
PRIVATE_REGISTRY=registry:5000
DOCKER_HUB_ORG=m4ucorp
DOCKER_IMG_NAME=$(shell dirname ${PWD}/.|rev|cut -d\/ -f 1|rev)
VERSION=$(shell cat kraken/__version__.py|cut -d\  -f3)

all: virtualenv install docker-push-private

virtualenv:
	virtualenv -p python3.6 venv/
	./venv/bin/python -m pip install -r requirements
install:
	./venv/bin/python setup.py install

docker-build:
	docker run --rm -v $(shell pwd):/worker -w /worker ${PYTHON_BASE_IMAGE}:${PYTHON_BASE_IMAGE_VERSION} pip install -t packages -r requirements
	docker build -t ${DOCKER_IMG_NAME}:${VERSION} .
	docker tag ${DOCKER_IMG_NAME}:$(shell cat kraken/__version__.py | cut -d"=" -f2| sed 's/\"//g'|sed 's/\ //g') ${DOCKER_IMG_NAME}:${VERSION}

docker-push-private: docker-build
	docker tag ${DOCKER_IMG_NAME}:${VERSION} ${PRIVATE_REGISTRY}/${DOCKER_IMG_NAME}:${VERSION}
	docker push ${PRIVATE_REGISTRY}/${DOCKER_IMG_NAME}:${VERSION}

docker-push-hub: docker-build
	docker tag ${DOCKER_IMG_NAME}:${VERSION} ${DOCKER_HUB_ORG}/plataformas-${DOCKER_IMG_NAME}:${VERSION}
	docker push ${DOCKER_HUB_ORG}/plataformas-${DOCKER_IMG_NAME}:${VERSION}

docker-push: docker-push-private docker-push-hub

test:
	docker run --rm -v $(shell pwd):/worker -e "PYTHONPATH=/worker/packages" -w /worker iron/python:3 python3 -m kraken

clean:
	sudo rm -rf venv/*
	sudo rm -rf dist build
	sudo rm -rf kraken.egg-info
	find . -name __pycache__ | xargs -I {} sudo rm -rf {}
	find . -name *.pyc |xargs -I {} sudo rm -rf {}
	sudo rm -rf packages

