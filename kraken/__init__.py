__name__ = 'kraken'
__all__ = ['ec2', 'ebs', 'ami', 'r53']
from .ec2 import ec2
from .ebs import ebs
from .ami import ami
from .r53 import r53
