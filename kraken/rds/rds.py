from boto3 import client, resource


class RDS:

    def __init__(self, region: str='us-east-1'):
        self.client = client('rds')
        __iam = resource('iam')
        self.region = region
        self._account_id = __iam.CurrentUser().arn.split(':')[4]
        self.db_arn_mask = 'arn:aws:rds:{region}:{account_id}:db:{db_instance_name}'
        self.response_cache = None

    def __aws_tag_list_into_dict(self):
        pass

    def __rds_tags(self, db_id: str=None) -> dict:
        """

        :return:
        """
        dbs = {}
        ids = []

        if db_id is None:
            ids = self.list_rds_identifiers()
        else:
            ids = db_id

        for i in ids:
            tags = []
            db_arn = str(self.db_arn_mask).format(region=self.region, account_id=self._account_id, db_instance_name=i)
            tag_resources = self.client.list_tags_for_resource(ResourceName=db_arn)
            for tag in tag_resources['TagList']:
                tags.append({tag['Key']: tag['Value']})
            dbs[i] = tags

        return dbs

    def __has_tag(self, tag: dict):
        pass

    def __is_unique(self, name):
        """

        :param name:
        :return:
        """
        response = self.client.describe_db_instances(Filters=[{'Name': 'db-instance-id', 'Values': [name]}])[
            'DBInstances']
        if len(response) == 1:
            return True
        else:
            return False

    def get_status(self, name: str, ignore_duplicated: bool=False) -> dict:
        """

        :param name:
        :param ignore_duplicated:
        :return:
        """

        if not ignore_duplicated and not self.__is_unique(name):
            return {'name': name, 'status': 'duplicated'}

        response = self.client.describe_db_instances(Filters=[{'Name': 'db-instance-id', 'Values': [name]}])['DBInstances']
        status = [
            {'name': dic['DBInstanceIdentifier'], 'status': dic['DBInstanceStatus']}
            for dic in response
        ].pop()
        return status

    def is_available(self, name: str):
        status_conditions = ['duplicated', 'stopped', 'starting']
        db = self.get_status(name)
        if db['status'] not in status_conditions:
            return True
        else:
            return False

    def list_rds_instances(self, filters: list=[]):
        """ Return a list of all DB identifiers

        :return response: AWS API response
        """
        response = self.client.describe_db_instances(Filters=filters)['DBInstances']
        return response

    def list_rds_identifiers(self):
        response = [rds['DBInstanceIdentifier'] for rds in self.list_rds_instances()]
        return response

    def list_rds_filtered_by_db_ids(self, ids: list=[]):
        response = self.list_rds_instances(filters=[{'Name': 'db-instance-id', 'Values': ids}])
        return response

    def start_rds_by_id(self, db_id: str) -> dict:
        """ Stop RDS DB Instance using the DBInstanceIdentifier

        :param db_id: DBInstanceIdentifier
        :return response: AWS API response
        """
        if not self.is_available(db_id):
            try:
                response = self.client.start_db_instance(DBInstanceIdentifier=db_id)
                return response
            except IOError as e:
                print('Fail to start db instance: {}'.format(e))
        else:
            return {'DBInstanceIdentifier': db_id, 'status': 'already_started'}

    def start_all_rds_instances(self):
        ids = self.list_rds_identifiers()
        response = [self.start_rds_by_id(i) for i in ids]
        return response

    def stop_rds_by_id(self, db_id: str):
        """ Stop RDS DB Instance using the DBInstanceIdentifier

        :param db_id: DBInstanceIdentifier
        :return response: AWS API response
        """
        if self.is_available(db_id):
            try:
                response = self.client.stop_db_instance(DBInstanceIdentifier=db_id)
                return response
            except IOError as e:
                print('Fail to stop db instance: {}'.format(e))
        else:
            return {'name': db_id, 'status': 'stopped'}

    def stop_all_rds_instances(self):
        ids = self.list_rds_identifiers()
        response = [self.stop_rds_by_id(i) for i in ids]
        return response


if __name__ == '__main__':
    from pprint import pprint, PrettyPrinter

    pp = PrettyPrinter(indent=2)
    rds = RDS()

    # db_inst_ids_by_tag = rds.list_all_rds_instances_by_tag()
    db_instances = rds.list_rds_instances()
    db_instances_ids = rds.list_rds_identifiers()
    # db_status = rds.get_status('rds-postgres-staging')

    # pp.pprint(db_instances)
    # pp.pprint(db_instances_ids)
    # pp.pprint(db_status)
    pp.pprint(rds.start_all_rds_instances())
    pp.pprint(rds.get_status('rds-m4wallet'))
