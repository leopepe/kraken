from sys import argv
from datetime import datetime, timedelta
from googleapiclient import sample_tools


class GCalendar:

    def __init__(self, calendar_id):
        self.calendar, self.flags = sample_tools.init(
            argv, 'calendar', 'v3', __doc__, __file__,
            scope='https://www.googleapis.com/auth/calendar.readonly'
        )
        self.time_frame_min_time = datetime.now()
        self.time_frame_max_time = self.time_frame_min_time + timedelta(hours=24)
        self.calendar_id = calendar_id

    def get_today_events(self, pageToken=None):
        events = self.calendar.service.events().list(
            calendarId=self.calendar_id,
            singleEvents=True,
            orderBy='startTime',
            timeMin=self.time_frame_min_time.strftime('%Y-%m-%dT%H:%M:%S-00:00'),
            timeMax=self.time_frame_max_time.strftime('%Y-%m-%dT%H:%M:%S-00:00'),
            pageToken=pageToken,
            ).execute()
        return events


if __name__ == '__main__':
    calendar = GCalendar('primary')
    print(calendar.get_today_events())
