from pprint import PrettyPrinter
import logging
import click
# from .ec2 import ec2
# from .ebs import ebs
# from .ami import ami
from kraken.ec2 import ec2
from kraken.ebs import ebs, snapshots
from kraken.ami import ami
from kraken.r53 import r53
from kraken.rds import rds
import json

ec2_client = ec2.EC2()
ebs_client = ebs.EBS()
snapshot_client = snapshots.SnapShots()
ami_client = ami.AMI()
r53_client = r53.R53()
rds_client = rds.RDS()

logger = logging.getLogger(__name__)


def logger_setup():
    # logging
    # create logger with 'spam_application'
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('kraken.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

def echo(msg: str, indent: int=2):
    pp = PrettyPrinter(indent=indent)
    pp.pprint(msg)


def date_handler(obj):
    # deal with non-serialized datetime format on json
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


@click.group()
@click.version_option()
def cli():
    """ Kraken demo

    Kraken is a destructive EC2 client.
    """


@cli.group()
def ec2():
    """ EC2 resources client. """


@ec2.command('ls')
@click.option('--debug', '-d', help='Enables debug mode')
@click.option('--state', '-s', nargs=1, default=None)
@click.option('--filters', '-f', multiple=False, type=str, default=None)
@click.option('--exclude', '-e', multiple=False, type=str, default=None)
def ec2_ls(state, debug, filters, exclude):
    """ List EC2 instances

        format: [{'InstanceId': 'i-88dea039', 'State': 'stopped', 'Type': 't2.micro', 'StartedAt': '2015-12-31 21:58:10', 'VpcId': 'vpc-b0d602d4'}]
    :param state:
    :param debug:
    :param filters:
    :param exclude:
    """
    # setting instances
    instances = ec2_client.list(state=state, exclude=exclude, filters=filters)
    click.echo(json.dumps(instances, ensure_ascii=False, indent=2))


@ec2.command('rm')
@click.option('--ids', '-i', type=str, help='List of running instances ids to be terminated')
@click.option('--running/--stopped', default=False, help='Terminate all instances running')
@click.option('--exclude', '-e', default=False, type=str, help='The list of ids must be separated by one white space')
def ec2_rm(ids, running, exclude):
    """ Terminate ec2 instances.

    :param ids: list of instance ids to terminated
    :param exclude
    :param running
    """
    if running:
        instances = ec2_client.list(state='running', exclude=exclude)
        ids = [
            instance['InstanceId']
            for instance in instances
        ]
        result = ec2_client.terminate(ids) + ' [instance was running]'

    elif ids:
        result = ec2_client.terminate(ids) + ' [instance ids terminated]'

    else:
        instances = ec2_client.list(state='stopped', exclude=exclude)
        ids = [
            instance['InstanceId']
            for instance in instances
        ]
        result = ec2_client.terminate(ids) + ' [instance was stopped]'

    click.echo('Termination result: {}'.format(result))


@ec2.command('stop')
@click.option('--ids', '-i', type=str, help='List of running instances ids to be stopped')
@click.option('--all/--not-all', default=False, help='Stop all instances running')
@click.option('--exclude', '-e', default=False, type=str, help='The list of ids must be separated by one white space')
def ec2_stop(ids, all, exclude):
    """ Stop ec2 instance

    :param ids
    :param all
    :param exclude: list of excluded ids
    :rtype str
    :return String containing the result of the instances stop:
    """
    if ids:
        result = ec2_client.stop(ids)
        click.echo('Stop result:\n {}'.format(result))
    else:
        instances = ec2_client.list(state='running', exclude=exclude)
        ids = [
            instance['InstanceId']
            for instance in instances
        ]
        result = ec2_client.stop(ids)
        click.echo('Stop result: {}'.format(result))


@cli.group()
def ebs():
    """ Manages EBS resources. """


@ebs.command('describe')
@click.argument('ids')
def ebs_describe(ids):
    """ Describe ebs volumes
    :param ids
    """
    ebs_client.get_volume_by_id(ids=ids)


@ebs.command('ls')
@click.option('--available/--all', default=False)
def ebs_ls(available):
    """ List EBS volumes.
    :param available
    """
    if available:
        # get_available_volumes must be verbose
        available = ebs_client.get_available_volumes(verbose=True)
        click.echo(
            'All available volumes: {}'.format(json.dumps(available, ensure_ascii=True, indent=2, default=date_handler))
        )
    else:
        all_volumes = ebs_client.get_all_volumes(verbose=True)
        click.echo(
            'All volumes: {}'.format(json.dumps(all_volumes, ensure_ascii=True, indent=2, default=date_handler))
        )


@ebs.command('rm')
@click.option('--available/--candidate', default=False)
def ebs_rm(available):
    """ Destroy EBS volumes. Default state = available """
    if available:
        click.echo(
            'Candidate volumes to be terminated: {}'.format(
                json.dumps(ebs_client.get_available_volumes(verbose=True), indent=2, default=date_handler)
            )
        )
        ebs_client.terminate_available()
    else:
        terminated = ebs_client.terminate_candidates()
        if terminated:
            click.echo('Ye ebs volumes deleted!')
        else:
            click.echo('ARR! Error deleting volumes!')


@cli.group()
def snapshot():
    """ Manages Snapshot resources. """


@snapshot.command('ls')
@click.option('--ids', '-i', is_flag=True)
@click.option('--orphans', '-o', is_flag=True)
def snapshot_ls(ids, orphans):
    if ids:
        click.echo('{}'.format(snapshot_client.list_my_snapshots_ids()))
    elif orphans:
        click.echo('{}'.format(snapshot_client.list_orphan_snapshots_ids()))
        

@snapshot.command('rm')
@click.option('--orphans', '-o', is_flag=True, default=True)
@click.option('--id', '-i')
def snapshot_rm(id, orphans):
    resp = None

    if orphans:
        resp = snapshot_client.delete_orphans()
    if id:
        resp = snapshot_client.delete_snapshot(id)

    if not resp:
        click.echo('No orphan snapshots where found')
    else:
        click.echo('Deleted snapshot IDs: {}'.format(resp))


@cli.group()
def ami():
    """ Manages EBS resources. """


@ami.command('ls')
@click.option('--all', '-a', is_flag=True, default=True)
@click.option('--project', '-p', default=False)
@click.option('--unused', '-u', default=False)
@click.option('--inuse', '-i', default=False)
@click.option('--except', '-e', type=str, default='infra-base')
@click.option('--last', '-l', type=int, default=False, help='list last X AMIS')
@click.option('-m', is_flag=True)
def ami_ls(all, project, unused, inuse, except_project, last, m):
    if all:
        images = ami_client.map_images_by_projects()
    elif project and last:
        images = ami_client.list_amis_ids_by_project(project=project)[-last:]
    elif project:
        images = ami_client.list_amis_ids_by_project(project=project)
    elif unused:
        images = ami_client.list_my_inactive_amis_ids()
    elif inuse:
        images = ami_client.list_my_active_amis_ids()
    elif last:
        images = ami_client.map_images_by_projects()[project][-last:]
    elif project and unused:
        images = set(ami_client.map_images_by_projects()[project]) - set(ami_client.list_my_active_amis_ids())
    elif except_project:
        images = set(ami_client.list_my_inactive_amis_ids()) - set(ami_client.map_images_by_projects()[project])
    elif map:
        images = ami_client.map_images_by_projects()
    else:
        images = ami_client.list_my_amis_ids()

    echo('Images: {}'.format(images))


@ami.command('rm')
@click.option('--project', type=str, default='infra-base')
@click.option('--keep_last', type=int, default=False, help='list last X')
@click.option('--days_old', type=int, default=False, help='Number of days old')
def ami_rm(days_old, project, keep_last):
    # temp
    import datetime
    delta = datetime.datetime.now() - datetime.timedelta(days=days_old)
    images = []

    if days_old:
        images = [
            image_id
            for image_id in ami_client.list_my_inactive_amis_ids()
            if datetime.datetime.strptime(ami_client.ec2.Image(image_id).creation_date, "%Y-%m-%dT%H:%M:%S.000Z") > delta
        ]
        ami_client.delete_ami_by_age(days_old=days_old)
    elif project and keep_last:
        images.append(ami_client.delete_amis_by_project(project=project, keep_last=keep_last))
    elif keep_last:
        images.append('INFORM THE NAME OF THE PROJECT')
    else:
        for ami in ami_client.list_my_inactive_amis_ids():
            images.append(ami_client.delete_ami_by_id(ami))

    click.echo('Deleted images: {}'.format(images))


@cli.group()
def r53():
    """ Manages Route53 RecordSets. """


@r53.command('ls')
# @click.option('--zone_id', '-z', type=str)
def r53_ls_zones():
    click.echo(json.dumps(r53_client.get_current_zones()))


@cli.group()
def rds():
    """ Manages RDS Resources """


@rds.command('ls')
@click.option('--ids', is_flag=True)
@click.option('-p', '--pretty', is_flag=True, default=True)
def rds_ls(ids, pretty):
    resp = str()

    if ids:
        resp = rds_client.list_rds_identifiers()
    elif not ids:
        resp = rds_client.list_rds_instances()

    if pretty:
        click.echo('RDS DBInstances: ')
        echo(resp)
    else:
        click.echo('RDS DBInstances: {}'.format(resp))


@rds.command('stop')
@click.option('--name', default=False)
@click.option('--all/--no-all', is_flag=True)
def rds_stop(name, all):
    resp = str()
    if name:
        resp = rds_client.stop_rds_by_id(db_id=name)
    if all:
        resp = rds_client.stop_all_rds_instances()

    echo('RDS DBInstances: {}'.format(resp))


@rds.command('start')
@click.option('--name', type=str, default=False)
@click.option('--all/--no-all', is_flag=True)
def rds_start(name, all):
    resp = str()
    if name:
        resp = rds_client.start_rds_by_id(db_id=name)
    if all:
        resp = rds_client.start_all_rds_instances()

    echo('RDS DBInstances: {}'.format(resp))

if __name__ == '__main__':
    cli()
