import boto3
import datetime
from kraken import ec2

"""
TODO:
 - List all AMI older than X Months
 - List all AMI by TAG
"""


class AMIError(RuntimeError):
    """ AMI generic ERROR

    """
    pass


class DeregisterError(AMIError):
    """ Could not deregister image

    """
    pass


class AMI:
    def __init__(self, region_name: str='us-east-1', profile_name: str=None):
        # default region us-east-1 virginia
        self.projects = None
        self._session = self.__open_session(region_name, profile_name)
        self.ec2 = self._session.resource('ec2')

        __iam = boto3.resource('iam')
        self._account_id = __iam.CurrentUser().arn.split(':')[4]

        self.today = datetime.datetime.now()
        self.sorted_images_by_date = None

    @staticmethod
    def __open_session(region, profile):
        if profile:
            return boto3.session.Session(region_name=region, profile_name=profile)
        else:
            return boto3.session.Session(region_name=region)

    @staticmethod
    def __sort_amis(amis):
        return sorted(amis, key=lambda image: image.creation_date)

    def __list_all_projects(self) -> list:
        """ ami by project
        :rtype: list
        :return: available projects set
        """
        """
        projects = set()
        for image_id in self.list_my_inactive_amis_ids():
            # if self.ec2.Image(image_id).tags:
                for tag in self.ec2.Image(image_id).tags:
                    if tag['Key'] == 'project':
                        projects.add(tag['Value'])
        return projects
        """
        try:
            projects = [
                tag['Value']
                for ami in self.get_amis_owned_by_me() if ami.tags
                for tag in ami.tags if tag['Key'] == 'project'
            ]
            return projects
        except RuntimeError as e:
            raise('ERROR generating projects list based on tag name project'.format(e))

    def __timedelta_days_ago(self, days) -> datetime:
        return self.today - datetime.timedelta(days=days)

    def __get_amis_by_filter(self, filters: list, sort: bool=False) -> list:
        # return self.__sort_amis(self.ec2.images.filter(Filters=filters))
        return self.ec2.images.filter(Filters=filters)

    def get_amis_by_project(self, project: str=None) -> list:
        return self.__get_amis_by_filter(filters=[{'Name': 'tag:project', 'Values': [project]}])

    def get_amis_by_tags(self, tags: list) -> list:
        filters = []
        for tag in tags:
            filters.append({'Name': 'tag-key', 'Values': sorted(tag.items(),key=lambda x: x[1])[0][0]})
            filters.append({'Name': 'tag-value', 'Values': sorted(tag.items(), key=lambda x: x[1])[0][1]})

        return self.__get_amis_by_filter(filters=filters)

    def get_amis_owned_by_me(self):
        return self.__get_amis_by_filter(filters=[{'Name': 'owner-id', 'Values': [self._account_id]}])

    def list_my_amis_ids(self):
        return [image.image_id for image in self.get_amis_owned_by_me()]

    def list_my_active_amis_ids(self):
        return [instance.image_id for instance in self.ec2.instances.all()]

    def list_my_inactive_amis_ids(self):
        return set(self.list_my_amis_ids()) - set(self.list_my_active_amis_ids())

    def list_amis_ids_by_tags(self, tags: list) -> list:
        return [image.id for image in self.get_amis_by_tags(tags=tags)]

    def map_amis_ids_by_date_filtered_by_tags(self, tags: list) -> list:
        return [{'id': image.id, 'created_at': image.creation_date} for image in self.get_amis_by_tags(tags=tags)]

    def map_images_by_projects(self) -> dict:
        amis = {}
        for project in self.__list_all_projects():
            amis[project] = self.get_amis_by_project(project=project)
        return amis

    def list_amis_ids_by_project(self, project: str) -> list:
        return [image.id for image in self.get_amis_by_project(project=project)]

    def list_latest_amis(self, project: str, last: int=3) -> list:
        candidates = []
        for img in self.list_amis_ids_by_project(project=project)[last:]:
            candidates.append(img)

        return candidates

    def delete_ami_by_age(self, days_old: int = 90):
        """ deregister amis older then int days_old

        :param days_old: int
        :return: void()
        """
        for image_id in self.list_my_inactive_amis_ids():
            created_date = datetime.datetime.strptime(
                self.ec2.Image(image_id).creation_date, "%Y-%m-%dT%H:%M:%S.000Z"
            )
            if created_date > self.__timedelta_days_ago(days=days_old):
                try:
                    # self.ec2.Image(id).deregister()
                    print('AMIs older then {0} days: {1} not in use'.format(days_old, image_id))
                except IOError as e:
                    raise DeregisterError('Could not deregister image: {}'.format(e))

    def delete_amis_by_project(self, project: str, keep_last: int=2):
        candidates = []
        active_amis = self.list_my_active_amis_ids()
        amis = [ami_id for ami_id in self.list_amis_ids_by_project(project=project) if ami_id not in active_amis]
        idx = len(amis) - keep_last
        for img in amis[:idx]:
            candidates.append(img)
            self.ec2.Image(img).deregister()

        return candidates

    def delete_ami_by_id(self, ami_id) -> dict:
        response = self.ec2.Image(ami_id).deregister()
        return response


if __name__ == '__main__':
    ami = AMI()
    # print('AMIs not being used: ', ami.list_my_inactive_amis_ids())
    # print('AMIs being used: ', ami.list_my_active_amis_ids())
    # print('AMIs of project X: ', ami.list_amis_ids_by_project('infra-base'))
    # print('AMIs with tags {project: mesos-slave}: ', ami.list_amis_ids_by_tags([{'project': 'novaplataforma-zuum'}]))
    # print('AMIs mapped by project: ', ami.map_images_by_projects())
    # print(ami.delete_ami_by_age(days_old=90))
    print(ami.delete_amis_by_project(project='m4wallet', keep_last=2))
