import boto3
# from kraken import ec2
import re


class SnapShots:
    def __init__(self):
        self.ec2 = boto3.resource('ec2')
        iam = boto3.resource('iam')
        self.account_id = iam.CurrentUser().arn.split(':')[4]

    def __description(self, snapshot_id):
        try:
            return self.ec2.Snapshot(snapshot_id).description
        except IOError as e:
            raise ('{}'.format(e))

    def __match_image(self, snapshot_id: str) -> tuple:
        error = (False, None)
        r = re.match(r"Created by.*for (ami-.*) from.*", self.__description(snapshot_id))
        if r:
            if r.groups()[0] in self.list_my_images_ids():
                return error
            else:
                return True, r.groups()[0]
        else:
            return error

    def __match(self, images_ids: list, snapshot_ids: list):
        orphans = []
        for snap in snapshot_ids:
            r = self.__match_image(snap)
            if r[0]:
                orphans.append(r)
        return orphans

    def list_my_images_ids(self) -> list:
        try:
            return [image.image_id for image in self.ec2.images.filter(Owners=[self.account_id])]
        except IOError as e:
            raise ('{}'.format(e))

    def list_my_snapshots_ids(self) -> list:
        try:
            return [snapshot.snapshot_id for snapshot in self.ec2.snapshots.filter(OwnerIds=[self.account_id])]
        except IOError as e:
            raise ('{}'.format(e))

    def list_orphan_snapshots_ids(self) -> list:
        my_snapshots = self.list_my_snapshots_ids()
        return [snap_id for snap_id in my_snapshots if self.__match_image(snap_id)[0]]

    def delete_snapshot(self, snapshot_id: str):
        resp = self.ec2.Snapshot(snapshot_id).delete()
        return resp

    def delete_orphans(self):
        deleted = []
        candidates_ids = self.list_orphan_snapshots_ids()
        for candidate in candidates_ids:
            # self.ec2.Snapshot(candidate).delete()
            # deleted.append(candidate)
            deleted.append(self.delete_snapshot(candidate))
        return deleted


if __name__ == '__main__':
    snapshot = SnapShots()
    candidates = snapshot.list_orphan_snapshots_ids()
    # r = snapshot.delete_orphans()
    print(candidates)
    # print(r)
