FROM python:3.6.2-slim

COPY ./kraken /app/kraken
COPY ./packages /app/packages
ENV PYTHONPATH /app/packages
WORKDIR /app
ENTRYPOINT ["python3", "-m", "kraken"]

